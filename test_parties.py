import unittest

import parties


class MyTestCase(unittest.TestCase):
    def test_fetch_governments(self):
        em = parties.ElectionMetadata()
        em.fetch_all()
        self.assertEqual(em.governments["Saarland"][0], "SPD")
        self.assertEqual(len(em.governments["Saarland"]), 1)
        self.assertEqual(len(em.governments["Bayern"]), 2)

    def test_party_states(self):
        em = parties.ElectionMetadata()
        em.fetch_all()
        self.assertGreater(len(em.get_party_states()["SPD"]), 2)  # SPD is in the government of many states
        self.assertEqual(1, len(em.get_party_states()["FW"]))  # This party exists only in 1 state


if __name__ == '__main__':
    unittest.main()
