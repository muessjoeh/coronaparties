import datetime
import logging
import os
import openpyxl  # read Excel files
import matplotlib.pyplot as plt
import requests


PARTY_COLORS = {
    # The color associated to a specific party when drawing it
    'CDU/CSU': 'k',
    'SPD': 'r',
    'Grüne': 'limegreen',
    'FDP': 'yellow',
    'Linke': 'm',
}

NORMED_PARTY_NAMES = {
    # Use always the same name, regardless of its source
    'CDU/CSU': 'CDU/CSU',  # otherwise, there would be a key error
    'SPD': "SPD",
    "GRÜNE": "Grüne",
    'FDP': "FDP",
    "DIE LINKE": "Linke",
    'AfD': "AfD"
}


class CovidDeaths:
    """The number of Covid19 associated deaths per state"""
    def __init__(self, state_name, year, week, cases):
        self.cases = int(cases)
        self.state_name = state_name
        #  Just take the monday, since the values are not that granular anyway
        self.date = datetime.datetime.strptime(f"{year} {week} 1", "%G %V %u")

    def __repr__(self):
        return f"{self.state_name} {self.date} {self.cases}"


class CachedURL:
    """The data is fetched from the given file, or the given URL if the file does not exist.
    In this case, the file is created and filled with the URL content"""

    def __init__(self, cache_file, url):
        self.cache_file = cache_file
        self.url = url

    def fill_cache(self):
        if os.path.exists(self.cache_file):
            logging.debug(f"The cache file {self.cache_file} exists. Use it")
            # Return the file content of the cache file
            with open(self.cache_file, 'rb') as cache:
                return cache.read()
        else:
            logging.debug(f"The cache file '{self.cache_file}' does not exist. Create it")
            # Fetch content of the URL
            content = requests.get(self.url)
            logging.debug(f'Got the content from the web of length {len(content.content)}')
            # Write the content into the cache file
            with open(self.cache_file, 'bw') as cache:
                cache.write(content.content)
                logging.debug('Wrote the content into the cache file')
            # Return the content
            return content.content


class ElectionMetadata:
    def __init__(self):
        """The work horse"""
        self.governments = {}  # The government parties of each state
        self.corona_deaths_states = {}  # The number of corona deaths per state

        self.renewables = {}

    def fetch_governments(self):
        """Fetch the governments in each federal state (hard coded (no historic data found);
        data from https://de.wikipedia.org/wiki/Landesregierung_(Deutschland), 23.05.2022)"""
        self.governments = {
            "Baden-Württemberg": ("Grüne", "CDU/CSU"),
            "Bayern": ("CDU/CSU", "FW"),
            "Berlin": ("SPD", "Grüne", "Linke"),
            "Brandenburg": ("SPD", "CDU/CSU", "Grüne"),
            "Bremen": ("SPD", "Grüne", "Linke"),
            "Hamburg": ("SPD", "Grüne"),
            "Hessen": ("CDU/CSU", "Grüne"),
            "Mecklenburg-Vorpommern": ("SPD", "Linke"),
            "Niedersachsen": ("SPD", "CDU/CSU"),
            "Nordrhein-Westfalen": ("CDU/CSU", "FDP"),
            "Rheinland-Pfalz": ("SPD", "Grüne", "FDP"),
            "Saarland": ("SPD",),
            "Sachsen": ("CDU/CSU", "Grüne", "SPD"),
            "Sachsen-Anhalt": ("CDU/CSU", "SPD", "FDP"),
            "Schleswig-Holstein": ("CDU/CSU", "Grüne", "FDP"),
            "Thüringen": ("Linke", "SPD", "Grüne")
        }

    def fetch_corona_deaths_per_state(self):
        """Fetch the Corona deaths for a specific date as a dict state:cases"""

        state_synonym = {
            # Make the state names consistent with the rest of the code
            "NW": "Nordrhein-Westfalen",
            "BY": "Bayern",
            "BW": "Baden-Württemberg",
            "NI": "Niedersachsen",
            "HE": "Hessen",
            "RP": "Rheinland-Pfalz",
            "SN": "Sachsen",
            "BE": "Berlin",
            "SH": "Schleswig-Holstein",
            "BB": "Brandenburg",
            "ST": "Sachsen-Anhalt",
            "TH": "Thüringen",
            "HH": "Hamburg",
            "MV": "Mecklenburg-Vorpommern",
            "SL": "Saarland",
            "HB": "Bremen"
        }

        url = CachedURL("coronatote.xls",
                        "https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Projekte_RKI/"
                        "COVID-19_Todesfaelle.xlsx?__blob=publicationFile")

        url.fill_cache()
        with open(url.cache_file, "rb") as death_file:
            work_book = openpyxl.load_workbook(death_file)  # Read the file again

        sheet = work_book['COVID_Todesfälle_BL']

        deaths = []
        for row in sheet.rows:  # Take all of them
            if row[0].value == "Bundesland":  # Do not work on the header line
                continue
            cases = row[3].value
            if cases.startswith('<'):
                cases = cases[1:]  # Interpret e.g. '<4' as '4'
            ct = CovidDeaths(row[0].value, row[1].value, row[2].value, cases)
            deaths.append(ct)

        for d in deaths:
            real_key = state_synonym[d.state_name]
            if self.corona_deaths_states.get(real_key, None) is None:
                self.corona_deaths_states[real_key] = 0  # Not yet in the dict => create it
            self.corona_deaths_states[real_key] += d.cases  # Store it by the full name

    def fetch_renewables(self):
        """Return the percentage of the rate of renewables in percent for each state as a dict in the format
        state:(year, value).
        I would prefer the money invested per year, but I did not find these figures."""

        for year in range(2016, 2019+1):
            logging.debug(f"Processing energies of {year}")
            with open(f"lak-{year}.xls", "rb") as energy_file:
                work_book = openpyxl.load_workbook(energy_file)
            sheet = work_book['LAK']
            renewables_description = sheet.cell(row=5, column=9).value
            logging.debug(f"I5 is {renewables_description}")
            assert renewables_description == "Erneuerbare Energieträger"  # Is this really the correct sheet?

            for row in range(7, 22 + 1):
                state = sheet.cell(row=row, column=1).value
                logging.debug(f"state is {state}")
                renewables = sheet.cell(row=row, column=9).value
                logging.debug(f"renewables is {renewables}")
                if renewables == '.' or renewables == '...':
                    continue  # When there is no value for this year, ignore it
                # Only store the value if  it is existing.
                # The year is increasing, so this value is the most current one
                self.renewables[state] = int(renewables)
        self.renewables["Saarland"] = 0  # Unknown

    def fetch_all(self):
        self.fetch_governments()
        self.fetch_corona_deaths_per_state()
        self.fetch_renewables()

    def plot_values(self, what, label):
        shares = what()
        logging.debug(f"shares: {shares}")
        ordered_parties = {k: v for k, v in sorted(shares.items(), key=lambda item: item[1], reverse=True)}
        logging.debug(f"ordered parties: {ordered_parties}")
        ordered_colors = [PARTY_COLORS[party] for party in ordered_parties.keys()]
        my_dpi = 96  # Fits most screens
        plt.figure(figsize=(800/my_dpi, 500/my_dpi), dpi=my_dpi)  # Figure is a bit wider
        plt.ylabel(label)
        #plt.grid(True)
        plt.bar(ordered_parties.keys(), ordered_parties.values(), color=ordered_colors)
        plt.show()

    def process_values_per_state(self, fetch_method):  # this is a function that delivers the desired values
        # fixme
        share_of_party = {}
        count_of_party = {}
        for state in self.governments:
            for party in self.governments[state]:
                if party == "FW":
                    continue  # FW belongs only to "Sonstige"
                share_of_party[party] =\
                    share_of_party.get(party, 0) + fetch_method(party, state)
                # A death of a party governing in 1 state count more than one that governs in 4 states:
                count_of_party[party] = count_of_party.get(party, 0) + 1
        for party in share_of_party.keys():
            share_of_party[party] = share_of_party[party] / count_of_party[party]
        return share_of_party

    def helper_for_corona(self, party, state):
        logging.debug(f"Fetching covid figure for state {state}")
        return self.corona_deaths_states[state]

    def helper_for_renewables(self, party, state):
        logging.debug(f"Fetching renewable figure for state {state}")
        return self.renewables[state]

    def process_corona_values_per_state(self):
        return self.process_values_per_state(self.helper_for_corona)

    def process_renewable_values_per_state(self):
        return self.process_values_per_state(self.helper_for_renewables)

    def get_party_states(self):
        """Reverse the dict of governments (in: state:parties)"""
        party_states = {}
        for state, parties in self.governments.items():
            for party in parties:
                states = party_states.get(party, [])
                states.append(state)
                party_states[party] = states
        return party_states


def main():
    logging.basicConfig(level=logging.DEBUG)
    # Overwhelmingly verbose other loggers
    logging.getLogger('matplotlib').setLevel(logging.WARN)
    logging.getLogger('PIL').setLevel(logging.WARN)

    em = ElectionMetadata()
    em.fetch_all()
    em.plot_values(em.process_corona_values_per_state,
                   "Anzahl Coronatote\n(gewichtet nach Anzahl der Regierungsbeteiligungen)")
    em.plot_values(em.process_renewable_values_per_state,
                   "Energieverbrauch, der durch EE gedeckt wird [kwh]\n"
                   "(gewichtet nach Anzahl der Regierungsbeteiligungen)")


if __name__ == "__main__":
    main()
